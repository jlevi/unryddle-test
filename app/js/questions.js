(function ($) {
    // 'use strict';
    app.clearlocalStorage();
    var appData = {
        realisticArray: [],
        investigativeArray: [],
        artisticArray: [],
        socialArray: [],
        enterprisingArray: [],
        conventionalArray: [],
        realisticTotal: null,
        investigativeTotal: null,
        artisticTotal: null,
        socialTotal: null,
        enterprisingTotal: null,
        conventionalTotal: null,
        currentQuestion: 1,

    };

    var $root = $('html, body'); // cache root element
    var $formSlides = $('#formSlides'); // cache slider wrapper
    var $submitTop = $('#subscribe_top'); // cache subscribe top form
    var $submitBottom = $('#subscribe_bottom'); // cache subscribe bottom form
    var $counterValue = $('.counterValue');
    var $getResult = $('#getResult');
    var $personalitySlides = $('#personalityForm');


    var current_fs, next_fs, previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches

    function goToSection(i) {
        // console.log($("fieldset:gt(" + i + ")"));
        // console.log($("fieldset:lt(" + i + ")"));
        $("fieldset:gt(" + i + ")").removeClass("current").addClass("next");
        $("fieldset:lt(" + i + ")").removeClass("current");
        setTimeout(function () {
            console.log($("fieldset").eq(i));
            $("fieldset").eq(i).removeClass("next").addClass("current active");
            if ($("fieldset.current").index() == 29) {
                $("#next").hide();
                $("#getResult").show();
            } else {
                $("#next").show();
                $("#getResult").hide();
            }
        }, 80);
    }

    function nextSection() {
        var i = $formSlides.find('fieldset.current').index();
        // var i = $personalitySlides.find('fieldset.current').index();
        if (i < 29) {
            console.log(i);
            var counter = parseInt($counterValue.text());
            counter += 1;
            console.log(counter);
            $counterValue.text(counter);
            goToSection(i + 1);
        } else {
          console.log(i);
        }
    }

    function prevSection() {
        var i = $formSlides.find('fieldset.current').index();
        // var i = $personalitySlides.find('fieldset.current').index();
        if(i > 0) {
            // console.log(parseInt($counterValue.text()));
            var counter = parseInt($counterValue.text());
            counter -= 1;
            $counterValue.text(counter);
            goToSection(i-1);
        }
    }

    function collateResults(elem){
        var personality = $(elem).closest('fieldset').data("personality");
        var rtotal, itotal, atotal, stotal , etotal, ctotal;
        if(personality === 'realistic') {
            appData.realisticArray.push(1);
            rtotal = parseInt($('.rtotal').text());
            rtotal = rtotal + 1;
            $('.rtotal').text(rtotal);
        }
        if(personality === 'artistic') {
            appData.artisticArray.push(1);
            atotal = parseInt($('.atotal').text());
            atotal = atotal + 1;
            $('.atotal').text(atotal);
        }
        if(personality === 'investigative'){
            appData.investigativeArray.push(1);
            itotal = parseInt($('.itotal').text());
            itotal = itotal + 1;
            $('.itotal').text(itotal);
        }
        if(personality === 'social') {
            appData.socialArray.push(1);
            stotal = parseInt($('.stotal').text());
            stotal = stotal + 1;
            $('.stotal').text(stotal);
        }
        if(personality === 'enterprising'){
            appData.enterprisingArray.push(1);
            etotal = parseInt($('.etotal').text());
            etotal = etotal + 1;
            $('.etotal').text(etotal);
        }
        if(personality === 'conventional'){
            appData.conventionalArray.push(1);
            ctotal = parseInt($('.ctotal').text());
            ctotal = ctotal + 1;
            $('.ctotal').text(ctotal);
        }
    }

    function subtractResults(elem) {
        var personality = $(elem).closest('fieldset').data("personality");
        var rtotal, itotal, atotal, stotal , etotal, ctotal;
        if(personality === 'realistic') {
            appData.realisticArray.splice(-1,1);
            rtotal = parseInt($('.rtotal').text());
            rtotal = rtotal - 1;
            $('.rtotal').text(rtotal);
        }
        if(personality === 'artistic') {
            appData.artisticArray.splice(-1,1);
            atotal = parseInt($('.atotal').text());
            atotal = atotal - 1;
            $('.atotal').text(atotal);
        }
        if(personality === 'investigative'){
            appData.investigativeArray.splice(-1,1);
            itotal = parseInt($('.itotal').text());
            itotal = itotal - 1;
            $('.itotal').text(itotal);
        }
        if(personality === 'social') {
            appData.socialArray.splice(-1,1);
            stotal = parseInt($('.stotal').text());
            stotal = stotal - 1;
            $('.stotal').text(stotal);

        }
        if(personality === 'enterprising'){
            appData.enterprisingArray.splice(-1,1);
            etotal = parseInt($('.etotal').text());
            etotal = etotal - 1;
            $('.etotal').text(etotal);
        }
        if(personality === 'conventional'){
            appData.conventionalArray.splice(-1,1);
            ctotal = parseInt($('.ctotal').text());
            ctotal = ctotal - 1;
            $('.ctotal').text(ctotal);
        }
    }

    function processResult(event) {
        let target = event.target;
        if($(target).attr('name') === 'no'){
            console.log('modify no');
            let $nextElem = $(target).next();
            if(!$nextElem.hasClass('outline')){
                $nextElem.addClass('outline');
                $(target).removeClass('outline');
            } else {
                $(target).removeClass('outline');
            }

        }

        if($(target).attr('name') === 'yes'){
            if($(target).hasClass('outline')){
                console.log('modify yes');
                $(target).removeClass('outline');
                collateResults(target);
            } else if(!$(target).hasClass('outline')){
                $(target).addClass('outline');
                subtractResults(target);
            } else{
                $(target).addClass('outline');
                subtractResults(target);
            }
        }
    }

    function storeResults(){

        app.save(app.realisticKey, appData.realisticArray);
        app.save(app.investigativeKey, appData.investigativeArray);
        app.save(app.artisticKey, appData.artisticArray);
        app.save(app.socialKey, appData.socialArray);
        app.save(app.enterprisingKey, appData.enterprisingArray);
        app.save(app.conventionalKey, appData.conventionalArray);

        window.location.assign('summary.html');
    }

    $(document).ready(function () {
        console.log('page ready....');
        $counterValue.text(appData.currentQuestion);

        $('#next').click(function (e) {
            nextSection();

        });

        $('#prev').click(function(e) {
            // console.log(e.target);
            prevSection();
        });

        $('input[type=button').click(processResult);

        $getResult.on('click', function(e) {
            e.preventDefault();
            $(this).addClass('spinner');
            storeResults();

        })

    });
}(jQuery))
