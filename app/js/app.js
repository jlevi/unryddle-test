var app = {
    realisticKey: 'R',
    investigativeKey: 'I',
    artisticKey: 'A',
    socialKey: 'S',
    enterprisingKey: 'E',
    conventionalKey: 'C',
    finalCodes: 'FC',

    save: function (key, data) {
        store(key, data);
    },

    load: function (key) {
        return store.get(key);
    },
    clearlocalStorage: function () {
        store(false);
    }
}
