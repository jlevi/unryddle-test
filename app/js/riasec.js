(function ($) {
    'use strict';

    var raisecData = {
        selectedInterests: app.load(app.finalCodes),
    }

    var $first = $('.first'),
        $second = $('.second'),
        $third = $('.third'),
        $interestCode = $('.interest-code');

    function displayCode(arr) {
        var first = arr[0].name.charAt(0);
        var second = arr[1].name.charAt(0);
        var third = arr[2].name.charAt(0);

        $first.text(first);
        $second.text(second);
        $third.text(third);

        displayActive();
    }

    function displayActive(){
        var fullpath = window.location.pathname.split('/');
        let pathName = fullpath[1];
        console.log(pathName);
    }

    $(document).ready(function () {
        console.log('page ready...');
        displayCode(raisecData.selectedInterests);

        // console.log(window.location.pathname.split('/'));

        $interestCode.on('click', function (e) {
            e.preventDefault();
            $(this).addClass('spinner');
            let code = $(this).text();

            if (code == 'R') {
                window.location.href = 'realistic.html';
            }

            if(code == 'I'){
                window.location.href = 'investigative.html';
            }

            if(code == 'A'){
                window.location.href = 'artistic.html';
            }

            if(code == 'S'){
                window.location.href = 'social.html';
            }

            if(code == 'E'){
                window.location.href = 'enterprising.html';
            }

            if(code == 'C'){
                window.location.href = 'conventional.html';
            }
        });
    });
}(jQuery))