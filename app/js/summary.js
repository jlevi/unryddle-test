(function ($) {
    var summaryData = {
        realisticTotal: app.load(app.realisticKey),
        investigativeTotal: app.load(app.investigativeKey),
        artisticTotal: app.load(app.artisticKey),
        socialTotal: app.load(app.socialKey),
        enterprisingTotal: app.load(app.enterprisingKey),
        conventionalTotal: app.load(app.conventionalKey),
        rValue: null,
        iValue: null,
        aValue: null,
        sValue: null,
        eValue: null,
        cValue: null,
        raisecArr: [],
        raisecFinal: [],
    }

    var $rtotal = $('.rtotal'),
        $itotal = $('.itotal'),
        $atotal = $('.atotal'),
        $stotal = $('.stotal'),
        $etotal = $('.etotal'),
        $ctotal = $('.ctotal'),
        $first = $('.first'),
        $second = $('.second'),
        $third = $('.third'),
        $interestCode = $('.interest-code');

    function displayCode(arr) {
        var first = arr[0].name.charAt(0);
        var second = arr[1].name.charAt(0);
        var third = arr[2].name.charAt(0);

        $first.text(first);
        $second.text(second);
        $third.text(third);

        // getCareerProfiles();
    }

    // function getCareerProfiles() {

    // }

    function getTotal(arr) {
        var currlength = 3;
        for (var i = 0; i < currlength; i++) {
            var oIndex;
            var res = Math.max.apply(Math, arr.map(function (o) {
                return o.value;
            }))
            var obj = arr.find(function (o, i) {
                oIndex = i;
                // console.log(oIndex)
                return o.value == res;
            })

            summaryData.raisecFinal.push(obj);
            arr.splice(oIndex, 1);
        }
        app.save(app.finalCodes, summaryData.raisecFinal);
        displayCode(summaryData.raisecFinal);
    }

    function displayResult(arr) {

        arr.forEach(function (item) {
            if (item.name === 'Realistic') {
                $rtotal.text(item.value);
            }
            if (item.name === 'Social') {
                $stotal.text(item.value);
            }
            if (item.name === 'Artistic') {
                $atotal.text(item.value);
            }
            if (item.name === 'Investigative') {
                $itotal.text(item.value);
            }
            if (item.name === 'Enterprising') {
                $etotal.text(item.value);
            }
            if (item.name === 'Conventional') {
                $ctotal.text(item.value);
            }
        });

        getTotal(arr);

    }

    function computeResults() {
        if (summaryData.realisticTotal.length == 0) {
            summaryData.rValue = 0;
        } else {
            summaryData.rValue = summaryData.realisticTotal.reduce(function (accumulator, currentValue) {
                return accumulator + currentValue;
            });
        }

        if (summaryData.investigativeTotal.length == 0) {
            summaryData.iValue = 0;
        } else {
            summaryData.iValue = summaryData.investigativeTotal.reduce(function (accumulator, currentValue) {
                return accumulator + currentValue;
            });
        }

        if (summaryData.artisticTotal.length == 0) {
            summaryData.aValue = 0;
        } else {
            summaryData.aValue = summaryData.artisticTotal.reduce(function (accumulator, currentValue) {
                return accumulator + currentValue;
            });
        }

        if (summaryData.socialTotal.length == 0) {
            summaryData.sValue = 0;
        } else {
            summaryData.sValue = summaryData.socialTotal.reduce(function (accumulator, currentValue) {
                return accumulator + currentValue;
            });
        }

        if (summaryData.enterprisingTotal.length == 0) {
            summaryData.eValue = 0;
        } else {
            summaryData.eValue = summaryData.enterprisingTotal.reduce(function (accumulator, currentValue) {
                return accumulator + currentValue;
            });
        }

        if (summaryData.conventionalTotal.length == 0) {
            summaryData.cValue = 0;
        } else {
            summaryData.cValue = summaryData.conventionalTotal.reduce(function (accumulator, currentValue) {
                return accumulator + currentValue;
            });
        }

        summaryData.raisecArr.push({
            name: 'Realistic',
            value: summaryData.rValue
        }, {
            name: 'Investigative',
            value: summaryData.iValue
        }, {
            name: 'Artistic',
            value: summaryData.aValue
        }, {
            name: 'Social',
            value: summaryData.sValue
        }, {
            name: 'Enterprising',
            value: summaryData.eValue
        }, {
            name: 'Conventional',
            value: summaryData.cValue
        });

        displayResult(summaryData.raisecArr);
    }

    computeResults();

    $(document).ready(function () {
        console.log('page ready.....');


        $interestCode.on('click', function (e) {
            e.preventDefault();
            $(this).addClass('spinner');
            let code = $(this).text();

            if (code == 'R') {
                window.location.href = 'realistic.html';
            }

            if(code == 'I'){
                window.location.href = 'investigative.html';
            }

            if(code == 'A'){
                window.location.href = 'artistic.html';
            }

            if(code == 'S'){
                window.location.href = 'social.html';
            }

            if(code == 'E'){
                window.location.href = 'enterprising.html';
            }

            if(code == 'C'){
                window.location.href = 'conventional.html';
            }
        });
    });
}(jQuery))
